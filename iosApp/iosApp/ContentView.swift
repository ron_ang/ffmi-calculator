import SwiftUI
import shared

struct ContentView: View {

    @State var heightFeet = ""
    @State var heightInch = ""
    @State var weight = ""
    @State var bodyFat = ""
    @State var result = ""
    var calculator = Calculator()

	var body: some View {
        NavigationView {
            Form {
                Text("FFMI is an alternative to body mass index which accounts for a person\'s muscle mass. The average male scores about 19 and it is hard to score above 25 naturally")
                
                Section(header: Text("Height")) {
                    HStack {
                        TextField("Feet", text: $heightFeet)
                        TextField("Inch", text: $heightInch)
                    }
                }
                
                Section(header: Text("Weight")) {
                    TextField("Weight", text: $weight)
                }
                
                Section(header: Text("Body Fat %")) {
                    TextField("Body Fat %", text: $bodyFat)
                }
                
                Section(header: Text("Result")) {
                    TextField("", text: $result).disabled(true)
                }

                Section {
                    Button(action: {
                        let calculator = Calculator()
                        let resultMap = calculator.calculateFFMI(heightInFeet: Int32(heightFeet) ?? 0,
                                                              heightInInches: Int32(heightInch) ?? 0,
                                                              weight: Double(weight) ?? 0,
                                                              bodyFatPercent: Double(bodyFat) ?? 0)
                        let ffmi = resultMap[Constants.init().FFMI] as! KotlinDouble
                        result = "FFMI: \(ffmi)"
                    }) {
                        Text("Compute")
                    }
                }
            }
            .navigationBarTitle("Fat Free Mass Index")
        }
	}
}

struct ContentView_Previews: PreviewProvider {
	static var previews: some View {
        ContentView()
	}
}
