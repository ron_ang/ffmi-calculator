package ang.ronillo.ffmicalculator

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import java.text.DecimalFormat
import me.about.ronillo.ffmi.Calculator
import me.about.ronillo.ffmi.Constants

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportFragmentManager.beginTransaction()
                .replace(R.id.container, StandardFormFragment.newInstance())
                .commit()
    }

    fun openFbGroup(view: View) {
        val browser = Intent(Intent.ACTION_VIEW, Uri.parse("https://goo.gl/PJG3mi"))
        startActivity(browser)
    }

    class StandardFormFragment : androidx.fragment.app.Fragment() {

        private val decimalFormat = DecimalFormat("#.###")

        override fun onActivityCreated(savedInstanceState: Bundle?) {
            super.onActivityCreated(savedInstanceState)

            val button = view?.findViewById<Button>(R.id.compute_bmi_button)
            button?.setOnClickListener {
                compute()
            }
        }

        private fun compute() {
            val weight = view?.findViewById<EditText>(R.id.__input_weight)
            val feet = view?.findViewById<EditText>(R.id.__input_height_foot)
            val inch = view?.findViewById<EditText>(R.id.__input_height_inch)
            val bodyFat = view?.findViewById<EditText>(R.id.__input_bodyfat)
            val result = view?.findViewById<TextView>(R.id.__result)

            try {
                val calculator = Calculator()
                val results = calculator.calculateFFMI(feet?.text.toString().toInt(), inch?.text.toString().toInt(), weight?.text.toString().toDouble(), bodyFat?.text.toString().toDouble())
                val outBuilder = StringBuilder()

                outBuilder.append("Fat free mass: " + decimalFormat.format(results[Constants.LEAN_WEIGHT]) + '\n')
                outBuilder.append("Body fat: " + decimalFormat.format(results[Constants.TOTAL_BODY_FAT]) + '\n')
                outBuilder.append("FFMI: " + decimalFormat.format(results[Constants.FFMI]) + '\n')
                outBuilder.append("Adjusted FFMI: " + decimalFormat.format(results[Constants.ADJUSTED_FFMI]) + '\n')

                result?.text = outBuilder.toString()
            } catch (e: Exception) {
                result?.text = e.message
            }
        }

        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                                  savedInstanceState: Bundle?): View? {
            return inflater.inflate(R.layout.fragment_standard, container, false)
        }

        companion object {

            /**
             * Returns a new instance of this fragment for the given section
             * number.
             */
            fun newInstance(): StandardFormFragment {
                val fragment = StandardFormFragment()
                return fragment
            }
        }
    }
}
