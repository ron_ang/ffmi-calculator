# README #

This README would normally document whatever steps are necessary to get this application up and running.
I assume that you are already knowledgeable about the Kotlin Multiplatform Mobile components, else [learn it](https://kotlinlang.org/docs/mobile/getting-started.html).


### What is this repository for? ###

* This repo contains the source code for the FFMI Calculator mobile application.
* 1.2
* [Get The Android App](https://play.google.com/store/apps/details?id=ang.ronillo.ffmicalculator)


### How do I get set up? ###

* Install Android Studio Arctic Fox
* Install XCode 12.4
* Install CocoaPods 1.8.4
* Open the project


### Project Structure ###
* The androidApp contains the Android codes.
* The iosApp contains the Swift codes. You can edit with XCode.
* The shared folder contains common code between iOS and Android.


### Contribution guidelines ###

* Enhance the iOS project
* Code review


### Who do I talk to? ###

* Repo owner or admin
* [LinkedIn](https://linkedin.com/in/ronillo-ang)