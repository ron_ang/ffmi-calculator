package me.about.ronillo.ffmi

import kotlin.math.pow

class Calculator {

    fun calculateFFMI(
        heightInFeet: Int,
        heightInInches: Int,
        weight: Double,
        bodyFatPercent: Double
    ): Map<String, Double> {
        val results = HashMap<String, Double>()

        val totalBodyFat = weight * (bodyFatPercent / 100)
        val leanWeight = weight * (1 - (bodyFatPercent / 100))
        val ffmi = (leanWeight / 2.2) / ((heightInFeet * 12.0 + heightInInches) * 0.0254).pow(2.0)
        val adjustedFfmi = ffmi + (6.3 * (1.8 - (heightInFeet * 12.0 + heightInInches) * 0.0254))

        results[Constants.TOTAL_BODY_FAT] = totalBodyFat
        results[Constants.LEAN_WEIGHT] = leanWeight
        results[Constants.FFMI] = ffmi
        results[Constants.ADJUSTED_FFMI] = adjustedFfmi

        return results
    }
}