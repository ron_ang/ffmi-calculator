package me.about.ronillo.ffmi

object Constants {

    const val TOTAL_BODY_FAT = "total_body_fat"
    const val LEAN_WEIGHT = "lean_weight"
    const val FFMI = "ffmi"
    const val ADJUSTED_FFMI = "adjusted_ffmi"
}