pluginManagement {
    repositories {
        google()
        gradlePluginPortal()
        mavenCentral()
    }
}

rootProject.name = "FFMI_Calculator"
include(":androidApp")
include(":shared")